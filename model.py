import torch.nn as nn
from torch.autograd import Variable
import torch
import numpy as np
import time

'''There's a lot more code in here than before, most of them were ideas I had that I didn't end up using due to poor performance, but provided as record'''
class RNNModel(nn.Module):
    """Container module with an encoder, a recurrent module, and a decoder."""

    def __init__(self, rnn_type, ntoken, ninp, nhid, nlayers, dropout=0.5, tie_weights=False, emb=None, freeze=False, gpu=False, nce=False, noise=None, noise_samples_arr=None, k=10, hsoft=False):
        super(RNNModel, self).__init__()
        self.drop = nn.Dropout(dropout)
        self.encoder = nn.Embedding(ntoken, ninp)
        if freeze:
            self.encoder.weight.requires_grad = False
        self.nce = nce
        self.nhid = nhid
        self.ntoken = ntoken
        self.hsoft = hsoft
        if rnn_type in ['LSTM', 'GRU']:
            self.rnn = getattr(nn, rnn_type)(ninp, nhid, nlayers, dropout=dropout)
        else:
            try:
                nonlinearity = {'RNN_TANH': 'tanh', 'RNN_RELU': 'relu'}[rnn_type]
            except KeyError:
                raise ValueError( """An invalid option for `--model` was supplied,
                                 options are ['LSTM', 'GRU', 'RNN_TANH' or 'RNN_RELU']""")
            self.rnn = nn.RNN(ninp, nhid, nlayers, nonlinearity=nonlinearity, dropout=dropout)
        if self.hsoft:
            self.decoder = h_softmax(nhid, ntoken)
        else:
            self.decoder = nn.Linear(nhid, ntoken)

        # Optionally tie weights as in:
        # "Using the Output Embedding to Improve Language Models" (Press & Wolf 2016)
        # https://arxiv.org/abs/1608.05859
        # and
        # "Tying Word Vectors and Word Classifiers: A Loss Framework for Language Modeling" (Inan et al. 2016)
        # https://arxiv.org/abs/1611.01462
        if tie_weights:
            if nhid != ninp:
                raise ValueError('When using the tied flag, nhid must be equal to emsize')
            self.decoder.weight = self.encoder.weight

        self.init_weights(emb, gpu)
        if self.hsoft:
            self.decoder.init_weights()

        if self.nce:
            self.criterion = nce_loss(self.decoder, noise, self.nhid, self.ntoken, noise_samples_arr, k=k, gpu=gpu)

        self.rnn_type = rnn_type
        self.nhid = nhid
        self.nlayers = nlayers

    def init_weights(self, emb=None, gpu=False):
        initrange = 0.1
        if emb is not None:
            if gpu:
                self.encoder.weight.data = torch.cuda.FloatTensor(emb)
            else:
                self.encoder.weight.data = torch.FloatTensor(emb)
        else:
            self.encoder.weight.data.uniform_(-initrange, initrange)
        if not self.hsoft:
            self.decoder.bias.data.fill_(0)
            self.decoder.weight.data.uniform_(-initrange, initrange)

    def forward(self, input, hidden, target=None):
        emb = self.drop(self.encoder(input))
        output, hidden = self.rnn(emb, hidden)
        output = self.drop(output)
        if self.nce:
            loss = self.criterion(output.view(output.size(0)*output.size(1), output.size(2)), target)
            return loss, hidden
        elif self.hsoft:
            clusts, words = self.decoder(output.view(output.size(0)*output.size(1), output.size(2)))
            return clusts,words,hidden
        else:
            decoded = self.decoder(output.view(output.size(0)*output.size(1), output.size(2)))
            return decoded.view(output.size(0), output.size(1), decoded.size(1)), hidden

    def init_hidden(self, bsz):
        weight = next(self.parameters()).data
        if self.rnn_type == 'LSTM':
            return (Variable(weight.new(self.nlayers, bsz, self.nhid).zero_()),
                    Variable(weight.new(self.nlayers, bsz, self.nhid).zero_()))
        else:
            return Variable(weight.new(self.nlayers, bsz, self.nhid).zero_())


'''infrequent self-normalization'''
'''trying to force the model to self-normalize its weights by penalizing the denominator of softmax'''
'''my code isn't fast enough as is'''
class self_norm_loss(nn.Module):
    def __init__(self, alph=1.5, gam=1, rate=10):
        super(self_norm_loss,self).__init__()
        self.alph = alph
        self.gam = gam
        self.counter = 0
        self.rate = rate

        #fraction of training examples to compute Z

    def forward(self, hid, target):
        Z = 0
        target_vals = target.data.numpy()
        if self.counter % self.rate ==0:
            mask = Variable(torch.FloatTensor(np.random.choice([0,1], size=(target.shape[0],1), p=[0.9, 0.1])), requires_grad=False)
            hidmask = torch.mul(mask,hid)
            Z = torch.log(torch.sum(torch.exp(hidmask)))
            Z = torch.mul(Z,Z)
        self.counter += 1

        loss = hid + (self.alph/self.gam)*Z

        scal_loss = 0.0
        for n in range(target.shape[0]):
            scal_loss += -loss[n,target_vals[n]]
        scal_loss /= n

        return scal_loss

'''Noise Contrastive estimation'''
'''nce loss code based on https://github.com/Stonesjtu/Pytorch-NCE/blob/master/nce.py but modified'''
'''additional inspiration from https://github.com/demelin/Noise-Contrastive-Estimation-NCE-for-pyTorch'''
'''also from reading the paper https://www.cs.toronto.edu/~amnih/papers/ncelm.pdf'''
class nce_loss(nn.Module):
    def __init__(self, linear, noise, input_size, output_size, noise_samples_arr, k=10, gpu=False):
        super(nce_loss, self).__init__()
        #use a linear layer
        self.ln = linear
        #unigram noise is not a model parameter, store unigram freqs
        self.register_buffer('noise', noise)
        #self.alias = AliasMethod(noise)
        self.gpu = gpu
        self.test_loss = nn.CrossEntropyLoss()

        #number of noise samples
        self.k = k
        self.sample_ind = 0
        #this is lnZ in the Mnih paper, which they said could be constant without affecting performance, 9 does well on average
        self.norm_term = 9
        self.noise_samples_arr = noise_samples_arr
        self.noise_samples_len = self.noise_samples_arr.shape[0]
        self.mode = 'train'

    def set_norm_const(self, const):
        self.norm_term = const

    def set_mode(self, mode):
        self.mode = mode

    def forward(self, hid, target):
        if self.mode=='train':
            loss = self.nce(hid, target)
        else:
            loss = self.ce(hid, target)
        return loss

    def nce(self, hid, target):
        num_examples = target.size(0)
        batchidx = np.arange(num_examples)
        kbatchidx = np.arange(num_examples*self.k)

        #these can be precomputed to maximize speed
        # (bsz,k)
        noise_samples = self.get_noise_samples(num_examples)

        #indexing hidden has to be optimized unlike indexing the noise probs (for backprop)
        prob_model, prob_noise_in_model = self.get_score(target, noise_samples, hid)

        # prob_model = hid[batchidx,target.data.view(-1)].view_as(target)
        # prob_noise_in_model = batched_hid[kbatchidx,noise_samples.data.view(-1)].view_as(noise_samples)

        if self.gpu:
            prob_target_in_noise = Variable(self.noise[target.data.view(-1)].view_as(target)).cuda()
            prob_noise = Variable(self.noise[noise_samples.data.view(-1)].view_as(noise_samples)).cuda()
        else:
            prob_target_in_noise = Variable(self.noise[target.data.view(-1)].view_as(target))
            prob_noise = Variable(self.noise[noise_samples.data.view(-1)].view_as(noise_samples))

        def safe_log(tensor):
            EPSILON = 1e-10
            return torch.log(EPSILON + tensor)

        model_loss = safe_log(prob_model / (prob_model + self.k * prob_target_in_noise))
        noise_loss = torch.sum(safe_log((self.k * prob_noise) / (prob_noise_in_model + self.k * prob_noise)), -1).squeeze()

        loss = - (model_loss + noise_loss)

        return loss.mean()

    def ce(self, hid, target):
        decoded = self.ln(hid)
        loss = self.test_loss(decoded, target)
        return loss

    '''if this isn't fast enough explore other options like multinomial approx'''
    def get_noise_samples(self, num_examples):
        noise_samples = np.zeros(num_examples*self.k, dtype=int)
        if self.sample_ind + num_examples*self.k >= self.noise_samples_len:
            np.random.shuffle(self.noise_samples_arr)
            self.sample_ind = 0
        noise_samples = self.noise_samples_arr[self.sample_ind:self.sample_ind + num_examples*self.k]
        self.sample_ind += num_examples*self.k

        if self.gpu:
            samples = Variable(torch.LongTensor(noise_samples.reshape((num_examples,self.k))).cuda())
        else:
            samples = Variable(torch.LongTensor(noise_samples.reshape((num_examples,self.k))))

        return samples

    '''straight from the github index_linear.py, better than my initial indexing'''
    def get_score(self, target_idx, noise_idx, hid):
        # flatten the following matrix
        hid = hid.view(-1, hid.size(-1))
        original_size = target_idx.size() # the size will be used to pack the output of indexlinear
        target_idx = target_idx.view(-1)
        noise_idx = noise_idx.view(-1, noise_idx.size(-1))

        indices = torch.cat([target_idx.unsqueeze(-1), noise_idx], dim=-1)

        # the pytorch's [] operator can't BP correctly with redundant indices
        # before version 0.2.0
        hid = hid.unsqueeze(1)
        target_batch = self.ln.weight.index_select(0, indices.view(-1)).view(*indices.size(), -1).transpose(1,2)
        bias = self.ln.bias.index_select(0, indices.view(-1)).view_as(indices).unsqueeze(1)
        out = torch.baddbmm(1, bias, 1, hid, target_batch).view(*original_size, -1)
        target_score, noise_score = out[:, 0], out[:, 1:]

        target_prob = target_score.sub(self.norm_term).exp()
        noise_prob = noise_score.sub(self.norm_term).exp()
        return target_prob, noise_prob

'''2 level hierarchical softmax'''
'''from https://arxiv.org/pdf/cs/0108006.pdf'''
'''found this didn't work so well in practice'''
class h_softmax(nn.Module):

    def __init__(self, nhid, noutput):
        super(h_softmax, self).__init__()
        self.nhid = nhid
        self.noutput = noutput
        self.per_class = np.int(np.ceil(np.sqrt(noutput)))
        self.nclass = np.int(np.ceil(self.noutput * 1./self.per_class))

        self.ln1 = nn.Linear(self.nhid, self.nclass) #first layer, maps hidden to cluster
        self.ln2 = nn.Linear(self.nhid, self.per_class) #maps hidden to word_idx
        self.lnh = nn.Linear(self.nclass, self.per_class) #maps cluster to word_idx

    def init_weights(self):
        initrange = 0.1
        self.ln1.bias.data.fill_(0)
        self.ln1.weight.data.uniform_(-initrange, initrange)
        self.ln2.bias.data.fill_(0)
        self.ln2.weight.data.uniform_(-initrange, initrange)
        self.lnh.bias.data.fill_(0)
        self.lnh.weight.data.uniform_(-initrange, initrange)

    def forward(self, hid):
        clusts = self.ln1(hid)
        hid2 = self.lnh(clusts)
        words = self.ln2(hid) + hid2

        return clusts, words

'''corresponding hierarchical softmax loss'''
class h_softmax_loss(nn.Module):

    def __init__(self, noutput, cluster_map):
        super(h_softmax_loss,self).__init__()
        self.noutput = noutput
        self.per_class = np.int(np.ceil(np.sqrt(noutput)))
        self.nclass = np.int(np.ceil(self.noutput * 1./self.per_class))
        self.clustmap = cluster_map
        self.lsmc = nn.LogSoftmax(dim=-1) #modeling logprob(cluster_idx|hidden)
        self.lsmw = nn.LogSoftmax(dim=-1) #modeling logprob(word_idx|hidden)

    def forward(self, clusts, words ,target):
        target_vals = target.data.numpy()
        logprobc = self.lsmc(clusts)
        logprobw = self.lsmw(words)

        loss = 0
        #this loop could probably be optimized better, but score is still terrible despite time
        for n in range(target.shape[0]):
            i = target_vals[n]
            loss += - logprobc[n,self.clustmap[0,i]] - logprobw[n,self.clustmap[1,i]]
        loss /= n

        return loss

'''instead of modeling the hierarchy computation as linear layers, why not have an RNN of very small size?'''
'''actually, weight sharing probably makes very little sense here'''
'''performance was even worse for this than the linear layers, also slows runtime'''
'''I've provided it for the idea, but haven't hooked it up in this version of model.py'''
class rnn_h_softmax(nn.Module):

    def __init__(self, ninp, noutput, nhid=32, nlayers=1):
        super(rnn_h_softmax, self).__init__()
        self.ninp = ninp
        self.nhid = nhid
        self.nout = np.int(np.ceil(np.sqrt(noutput)))
        self.noutput = noutput
        self.nlayers = nlayers

        self.lnx = nn.Linear(self.ninp, self.nhid)
        self.lnh = nn.Linear(self.nhid, self.nhid)
        self.lny = nn.Linear(self.nhid, self.nout)
        self.sig = nn.Sigmoid()

    def init_weights(self):
        initrange = 0.1
        self.lnx.bias.data.fill_(0)
        self.lnx.weight.data.uniform_(-initrange, initrange)
        self.lnh.bias.data.fill_(0)
        self.lnh.weight.data.uniform_(-initrange, initrange)
        self.lny.bias.data.fill_(0)
        self.lny.weight.data.uniform_(-initrange, initrange)
        pass

    def forward(self, inp):
        #first hidden state is zero?
        x1 = self.lnx(inp)
        h1 = self.sig(x1)
        y1 = self.lny(h1)

        x2 = self.lnx(inp)
        h2 = self.sig(x2 + self.lnh(h1))
        y2 = self.lny(h2)

        return y1, y2

'''from the following paper, basically only worries about approximating based on svd of weight matrix'''
'''http://papers.nips.cc/paper/7130-svd-softmax-fast-softmax-approximation-on-large-vocabulary-neural-networks.pdf'''
'''not applicable to what we want because you need trained W,b, only good at test-time'''
'''which is why it's unused in the code'''
class svd_softmax(nn.Module):
    def __init__(self, nhid, noutput, preview_width=32, k=10):
        super(svd_softmax, self).__init__()
        self.k = k
        self.nhid = nhid
        self.noutput = noutput

        #the preview width in the paper
        self.pw = self.nhid // 4

        self.ln = nn.Linear(self.nhid,self.noutput)
        self.lsm = nn.LogSoftmax(dim=-1)

    def init_weights(self):
        initrange = 0.1
        self.ln.bias.data.fill_(0)
        self.ln.weight.data.uniform_(-initrange, initrange)

    def forward(self, hid):
        U, S, V = torch.svd(self.ln.weight)
        B = torch.matmul(U,torch.diag(S))
        V = torch.t(V)
        h = hid.mm(V)
        z = h[:,:self.pw].mm(torch.t(B[:,:self.pw])) + self.ln.bias

        #find top values
        _, inds = torch.topk(z, k=self.k)
        indxs = inds.data.numpy()

        #recalculate top values
        for ind in indxs:
            for i in ind:
                z[:,i] = h.mv(B[i,:]) + self.ln.bias[i]

        logits = self.lsm(z)

        return logits
