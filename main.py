# coding: utf-8
import argparse
import time
import math
import torch
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import sys, os

import data
import model

#need this line for point
# torch.set_num_threads(1)

parser = argparse.ArgumentParser(description='PTB RNN/LSTM Language Model: Main Function')
parser.add_argument('--data', type=str, default='./data/ptb',
                    help='location of the data corpus')
parser.add_argument('--model', type=str, default='LSTM',
                    help='type of recurrent net (RNN_TANH, RNN_RELU, LSTM, GRU)')
parser.add_argument('--emsize', type=int, default=100,
                    help='size of word embeddings')
parser.add_argument('--nhid', type=int, default=128,
                    help='number of hidden units per layer')
parser.add_argument('--nlayers', type=int, default=1,
                    help='number of layers')
parser.add_argument('--lr', type=float, default=20,
                    help='initial learning rate')
parser.add_argument('--clip', type=float, default=0.25,
                    help='gradient clipping')
parser.add_argument('--epochs', type=int, default=5,
                    help='upper epoch limit')
parser.add_argument('--batch_size', type=int, default=32, metavar='N',
                    help='batch size')
parser.add_argument('--bptt', type=int, default=35,
                    help='sequence length')
parser.add_argument('--dropout', type=float, default=0.2,
                    help='dropout applied to layers (0 = no dropout)')
parser.add_argument('--tied', action='store_true',
                    help='tie the word embedding and softmax weights')
parser.add_argument('--seed', type=int, default=1111,
                    help='random seed')
parser.add_argument('--log_interval', type=int, default=200, metavar='N',
                    help='report interval')
parser.add_argument('--save', type=str,  default='model.pt',
                    help='path to save the final model')
'''parameters that I added'''
parser.add_argument('--decay', type=float, default=None,
                    help='scalar decay to divide learning rate by')
parser.add_argument('--selfnorm', action='store_true',
                    help='use infrequent self normalization')
parser.add_argument('--selfnormrate', type=int, default=10,
                    help='self normalization computed every [selfnormrate] times')
parser.add_argument('--pretrained', type=int, default=0,
                    help='use pretrained word embeddings, these need to match emsize')
parser.add_argument('--freeze', action='store_true',
                    help='freeze the embedding layer (only makes sense with --pretrained)')
parser.add_argument('--logfile', type=str,
                    help='path to logfile')
parser.add_argument('--gpu', action='store_true',
                    help='use a gpu')
parser.add_argument('--nce', action='store_true',
                    help='use noise contrastive estimation loss instead of softmax')
parser.add_argument('--noise_samples', type=int, default=10,
                    help='number of noise samples to draw with nce')
parser.add_argument('--norm_term', type=float, default=9.0,
                    help='lnZ constant for nce loss')
parser.add_argument('--hsoft', action='store_true',
                    help='use hierarchical softmax')
parser.add_argument('--stop_nce', type=int, default=5,
                    help='after this epoch #, switch to softmax')
parser.add_argument('--start_decay', type=int, default=0,
                    help='start_decay at this epoch')
parser.add_argument('--exp_decay', type=float, default=None,
                    help='k constant for exponential lr decay')
args = parser.parse_args()


# Set the random seed manually for reproducibility.
torch.manual_seed(args.seed)

###############################################################################
# Load data
###############################################################################

if args.logfile is not None:
    sys.stdout = open(args.logfile, 'w', encoding='utf-8')
    print(sys.argv)
corpus = data.Corpus(args.data)
pretrained_path = None
'''these vectors were downloaded from https://nlp.stanford.edu/projects/glove/ (Wikipedia 2014 + Gigaword 5)'''
if args.pretrained==50:
    pretrained_path = './glove.6B/glove.6B.50d.txt'
elif args.pretrained==100:
    pretrained_path = './glove.6B/glove.6B.100d.txt'
elif args.pretrained==200:
    pretrained_path = './glove.6B/glove.6B.200d.txt'
elif args.pretrained==300:
    pretrained_path = './glove.6B/glove.6B.300d.txt'

'''init pretrained embedding'''
if pretrained_path is not None:
    print('Fetching pretrained embedding')
    vecs = np.random.randn(10000,args.pretrained)
    with open(pretrained_path, 'r', encoding='utf-8') as f:
        lines = f.readlines()
        for line in lines:
            data = line.split()
            word = data[0]
            if word in corpus.dictionary.word2idx.keys():
                temp = np.array(data[1:])
                vecs[corpus.dictionary.word2idx[word],:] = temp.astype(np.float32)
else:
    vecs = None

if args.nce or args.hsoft:
    print('Computing unigram frequencies')
    '''precompute frequency of words on training data'''
    freqs = np.zeros(len(corpus.dictionary), dtype=np.float32)
    with open(os.path.join(args.data,'train.txt'), encoding='utf-8') as f:
        for line in f.readlines():
            words = line.split() + ['<eos>']
            for word in words:
                if word in corpus.dictionary.word2idx.keys():
                    freqs[corpus.dictionary.word2idx[word]] += 1
    freqs /= np.sum(freqs)
    if args.nce:
        print('Generating initial noise sample array')
        '''precompute array to sample unigram noise from'''
        total_len = len(corpus.dictionary)*1000
        noise_samples_arr = np.zeros(total_len, dtype=int)
        class_counts = np.round(freqs*total_len).astype(int)
        ind = 0
        for i in range(class_counts.shape[0]):
            noise_samples_arr[ind:ind+class_counts[i]] = i
            ind = ind+class_counts[i]
        np.random.shuffle(noise_samples_arr)

        if args.gpu:
            noise = torch.FloatTensor(freqs).cuda()
        else:
            noise = torch.FloatTensor(freqs)

    elif args.hsoft:
        sortedwords = np.argsort(freqs)

        #form the initial hierarchical softmax tree
        clustmap = np.zeros((2,10000))
        perclustpos = np.zeros(10000)
        for i in range(10000):
            clustmap[0,sortedwords[i]] = i // 100
            clustmap[1,sortedwords[i]] = i % 100
else:
    noise = None
    noise_samples_arr = None

# Starting from sequential data, batchify arranges the dataset into columns.
# For instance, with the alphabet as the sequence and batch size 4, we'd get
# ┌ a g m s ┐
# │ b h n t │
# │ c i o u │
# │ d j p v │
# │ e k q w │
# └ f l r x ┘.
# These columns are treated as independent by the model, which means that the
# dependence of e. g. 'g' on 'f' can not be learned, but allows more efficient
# batch processing.

def batchify(data, bsz):
    # Work out how cleanly we can divide the dataset into bsz parts.
    nbatch = data.size(0) // bsz
    # Trim off any extra elements that wouldn't cleanly fit (remainders).
    data = data.narrow(0, 0, nbatch * bsz)
    # Evenly divide the data across the bsz batches.
    data = data.view(bsz, -1).t().contiguous()
    return data

eval_batch_size = 10
if args.gpu:
    train_data = batchify(corpus.train, args.batch_size).cuda()
    val_data = batchify(corpus.valid, eval_batch_size).cuda()
    test_data = batchify(corpus.test, eval_batch_size).cuda()
else:
    train_data = batchify(corpus.train, args.batch_size)
    val_data = batchify(corpus.valid, eval_batch_size)
    test_data = batchify(corpus.test, eval_batch_size)

###############################################################################
# Build the model
###############################################################################

ntokens = len(corpus.dictionary)
if args.nce:
    print('NCE LOSS VERSION')
elif args.selfnorm:
    print('INFREQUENT SELF-NORMALIZATION VERSION')
    criterion = model.self_norm_loss(rate=args.selfnormrate)
elif args.hsoft:
    print("HierarchicalSoftmax VERSION (based on frequency clustering)")
    criterion = model.h_softmax_loss(ntokens,clustmap)
else:
    print('REGULAR SOFTMAX VERSION')
    criterion = nn.CrossEntropyLoss()
if args.gpu:
    model = model.RNNModel(args.model, ntokens, args.emsize, args.nhid, args.nlayers, args.dropout, args.tied,
    emb=vecs, freeze=args.freeze, gpu=args.gpu, nce=args.nce, noise=noise, noise_samples_arr=noise_samples_arr, k=args.noise_samples, hsoft=args.hsoft).cuda()
else:
    model = model.RNNModel(args.model, ntokens, args.emsize, args.nhid, args.nlayers, args.dropout, args.tied,
    emb=vecs, freeze=args.freeze, gpu=args.gpu, nce=args.nce, noise=noise, noise_samples_arr=noise_samples_arr, k=args.noise_samples, hsoft=args.hsoft)
# if args.gpu:
#     model = model.RNNModel(args.model, ntokens, args.emsize, args.nhid, args.nlayers, args.dropout, args.tied, emb=vecs, gpu=True).cuda()
# else:
#     model = model.RNNModel(args.model, ntokens, args.emsize, args.nhid, args.nlayers, args.dropout, args.tied, emb=vecs, gpu=False)

###############################################################################
# Training code
###############################################################################

def repackage_hidden(h):
    """Wraps hidden states in new Variables, to detach them from their history."""
    if type(h) == Variable:
        return Variable(h.data)
    else:
        return tuple(repackage_hidden(v) for v in h)


# get_batch subdivides the source data into chunks of length args.bptt.
# If source is equal to the example output of the batchify function, with
# a bptt-limit of 2, we'd get the following two Variables for i = 0:
# ┌ a g m s ┐ ┌ b h n t ┐
# └ b h n t ┘ └ c i o u ┘
# Note that despite the name of the function, the subdivison of data is not
# done along the batch dimension (i.e. dimension 1), since that was handled
# by the batchify function. The chunks are along dimension 0, corresponding
# to the seq_len dimension in the LSTM.

def get_batch(source, i, evaluation=False):
    seq_len = min(args.bptt, len(source) - 1 - i)
    data = Variable(source[i:i+seq_len], volatile=evaluation)
    target = Variable(source[i+1:i+1+seq_len].view(-1))
    return data, target


def evaluate(data_source):
    # Turn on evaluation mode which disables dropout.
    model.eval()
    if args.nce:
        model.criterion.set_mode('eval')
    total_loss = 0
    ntokens = len(corpus.dictionary)
    hidden = model.init_hidden(eval_batch_size)
    for i in range(0, data_source.size(0) - 1, args.bptt):
        data, targets = get_batch(data_source, i, evaluation=True)
        if args.nce:
            loss, hidden = model(data, hidden, target=targets)
            loss = loss.data
        else:
            if args.hsoft:
                output1, output2, hidden = model(data, hidden)
                loss = criterion(output1.view(-1, 100),output2.view(-1,100),targets).data
            else:
                output, hidden = model(data, hidden)
                output_flat = output.view(-1, ntokens)
                loss = criterion(output_flat, targets).data
        total_loss += len(data) * loss
        hidden = repackage_hidden(hidden)
    return total_loss[0] / len(data_source)


def train(nce_mode='train'):
    # Turn on training mode which enables dropout.
    model.train()
    if args.nce:
        if nce_mode=='eval':
            model.criterion.set_mode('eval')
        else:
            model.criterion.set_mode('train')
    total_loss = 0
    start_time = time.time()
    ntokens = len(corpus.dictionary)
    hidden = model.init_hidden(args.batch_size)
    for batch, i in enumerate(range(0, train_data.size(0) - 1, args.bptt)):
        data, targets = get_batch(train_data, i)
        # Starting each batch, we detach the hidden state from how it was previously produced.
        # If we didn't, the model would try backpropagating all the way to start of the dataset.
        hidden = repackage_hidden(hidden)
        model.zero_grad()
        if args.nce:
            loss, hidden = model(data, hidden, target=targets)
        else:
            if args.hsoft:
                output1, output2, hidden = model(data, hidden)
                loss = criterion(output1.view(-1, 100),output2.view(-1,100),targets)
            else:
                output, hidden = model(data, hidden)
                loss = criterion(output.view(-1, ntokens), targets)
        loss.backward()

        # `clip_grad_norm` helps prevent the exploding gradient problem in RNNs / LSTMs.
        torch.nn.utils.clip_grad_norm(model.parameters(), args.clip)
        for p in model.parameters():
            if p.requires_grad==True:
                p.data.add_(-lr, p.grad.data)

        total_loss += loss.data

        if batch % args.log_interval == 0 and batch > 0:
            cur_loss = total_loss[0] / args.log_interval
            elapsed = time.time() - start_time
            print('| epoch {:3d} | {:5d}/{:5d} batches | lr {:02.2f} | ms/batch {:5.2f} | '
                    'loss {:5.2f} | perplexity {:8.2f}'.format(
                epoch, batch, len(train_data) // args.bptt, lr,
                elapsed * 1000 / args.log_interval, cur_loss, math.exp(cur_loss)))
            total_loss = 0
            start_time = time.time()

# Loop over epochs.
lr = args.lr
best_val_loss = None
if args.nce:
    model.criterion.set_norm_const(args.norm_term)

# At any point you can hit Ctrl + C to break out of training early.
mode = None
if args.nce:
    mode = 'train'
try:
    for epoch in range(1, args.epochs+1):
        epoch_start_time = time.time()
        if epoch > args.stop_nce:
            mode='eval'
        train(mode)
        val_loss = evaluate(val_data)
        print('-' * 89)
        print('| end of epoch {:3d} | time: {:5.2f}s | valid loss {:5.2f} | '
                'valid perplexity {:8.2f}'.format(epoch, (time.time() - epoch_start_time),
                                           val_loss, math.exp(val_loss)))
        print('-' * 89)
        # Save the model if the validation loss is the best we've seen so far.
        if args.decay is None and args.exp_decay is None:
            if not best_val_loss or val_loss < best_val_loss:
                with open(args.save, 'wb') as f:
                    torch.save(model, f)
                best_val_loss = val_loss
            else:
                # Anneal the learning rate if no improvement has been seen in the validation dataset.
                lr /= 4.0
        else:
            if not best_val_loss or val_loss < best_val_loss:
                with open(args.save, 'wb') as f:
                    torch.save(model, f)
                best_val_loss = val_loss
            if epoch > args.start_decay:
                if args.exp_decay is not None:
                    lr = args.lr*math.exp(-args.exp_decay*epoch)
                elif args.decay is not None:
                    lr /= args.decay
except KeyboardInterrupt:
    print('-' * 89)
    print('Exiting from training early')

# Load the best saved model.
with open(args.save, 'rb') as f:
    model = torch.load(f)

# Run on test data.
test_loss = evaluate(test_data)
print('=' * 89)
print('| End of training | test loss {:5.2f} | test perplexity {:8.2f}'.format(
    test_loss, math.exp(test_loss)))
print('=' * 89)
